import React from 'react'
import { BackTop , Tooltip } from 'antd';
import { PhoneOutlined , FacebookOutlined , WechatOutlined } from "@ant-design/icons";
export default function PageRight() {
    const style = {
        width:'100px',
        height:'100px',
        backgroundColor:'rgba(112,111,111)',
        borderRadius:'50%',
        lineHeight:'100px',
        bottom:'200px',
        textAlign: 'center',
        color: 'white',
    }
    const icons = {
        fontSize:'4rem',
        right: '50px',
        color: "#fff",
        borderRadius: '50%',
        position:'fixed',
        bottom:'400px',
        backgroundColor:'#007BFF',
        cursor:'pointer'
    }
  return (
    <>
    <div style={{position:'relative'}}>
    <Tooltip title="Liên hệ qua số điện thoại" placement="left"><PhoneOutlined  style={{
        fontSize:'3.5rem',
        right: '80px',
        color: "#fff",
        borderRadius: '50%',
        position:'fixed',
        bottom:'400px',
        backgroundColor:'#007BFF',
        cursor:'pointer'
    }}></PhoneOutlined></Tooltip>
    <Tooltip title="Liên hệ Facebook" placement="left"><FacebookOutlined style={{
        fontSize:'3.5rem',
        right: '80px',
        color: "#fff",
        position:'fixed',
        bottom:'500px',
        backgroundColor:'#007BFF',
        cursor:'pointer'
    }}></FacebookOutlined></Tooltip>
    <Tooltip title="Liên hệ WeChat" placement="left"><WechatOutlined style={{
        fontSize:'3.5rem',
        right: '80px',
        color: "#fff",
        borderRadius: '50%',
        position:'fixed',
        bottom:'600px',
        backgroundColor:'#007BFF',
        cursor:'pointer'
    }}></WechatOutlined></Tooltip>
    </div>

    <BackTop style={{marginBottom:'100px'}}>
      <div style={style}>Back To Top</div>
    </BackTop>
    </>
  )
}
