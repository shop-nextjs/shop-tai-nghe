import React, { useState } from "react";
import "antd/dist/antd.css";
import { Button, Cascader, Checkbox, Form, Input, Select } from "antd";
import { useSelector } from "react-redux";
import { registerCustomer } from "../../services/api/registerInfo";
import { getCookie } from "react-use-cookie";
import { registerSuccess , warning , updateInfoError } from "../../services/message/registerInfo";


const { Option } = Select;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const Info = () => {
  const { province, district, ward } = useSelector(
    (reduxData) => reduxData.address
  );
  const [selectedProvince, setSelectedProvince] = useState("");
  const [selectedDistrict, setSelectedDistrict] = useState("");
  const [form] = Form.useForm();
  const getUsername = getCookie('username');


  const onFinish = (values) => {
    values.username = getUsername;
    registerCustomer(values).then((data) =>
      updateInfoSuccess()
    ).catch((err) => updateInfoError());
  };

  const onFinishFailed = () => {
    warning();
  }

  return (
    <Form
      {...formItemLayout}
      form={form}
      name="register"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      scrollToFirstError
    >
      <Form.Item
        name="firstName"
        label="Họ"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập họ!",
          },
        ]}
      >
        <Input/>
      </Form.Item>

      <Form.Item
        name="lastName"
        label="Tên"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập tên!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="email"
        label="E-mail"
        rules={[
          {
            type: "email",
            message: "Đây không phải email!",
          },
          {
            required: true,
            message: "Vui lòng nhập email!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="gender"
        label="Giới tính"
        rules={[
          {
            required: true,
            message: "Vui lòng chọn giới tính!",
          },
        ]}
      >
        <Select placeholder="Chọn giới tính">
          <Option value="Nam">Nam</Option>
          <Option value="Nữ">Nữ</Option>
          <Option value="Khác">Khác</Option>
        </Select>
      </Form.Item>
      <Form.Item
        name="province"
        label="Thành phố"
        rules={[
          {
            required: true,
            message: "Vui lòng chọn thành phố!",
          },
        ]}
      >
        <Select
          placeholder="Thành phố"
          onChange={(e) => setSelectedProvince(e)}
        >
          {province.map((data) => {
            return <Option value={data.id}>{data.name}</Option>;
          })}
        </Select>
      </Form.Item>
      <Form.Item
        name="district"
        label="Quận huyện"
        rules={[
          {
            required: true,
            message: "Vui lòng chọn quận huyện!",
          },
        ]}
      >
        <Select
          placeholder="Quận huyện"
          onChange={(e) => {
            setSelectedDistrict(e);
          }}
        >
          {district.map((data) => {
            if (data.provinceId == selectedProvince) {
              return <Option value={data.id}>{data.name}</Option>;
            }
          })}
        </Select>
      </Form.Item>
      <Form.Item
        name="ward"
        label="Xã phường"
        rules={[
          {
            required: true,
            message: "Vui lòng chọn xã phường!",
          },
        ]}
      >
        <Select placeholder="Xã phường">
          {ward.map((data) => {
            if (data.districtId == selectedDistrict) {
              return <Option value={data.id}>{data.name}</Option>;
            }
          })}
        </Select>
      </Form.Item>

      <Form.Item
        name="address"
        label="Địa chỉ"
        tooltip="Địa chỉ nhận hàng của bạn."
        rules={[
          {
            required: true,
            message: "Vui lòng nhập địa chỉ!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="phoneNumber"
        label="Số điện thoại"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập số điện thoại!",
          },
        ]}
      >
        <Input
          style={{
            width: "100%",
          }}
        />
      </Form.Item>
      <Form.Item
        name="agreement"
        valuePropName="checked"
        rules={[
          {
            validator: (_, value) =>
              value
                ? Promise.resolve()
                : Promise.reject(new Error("Phải đồng ý với quy định !")),
          },
        ]}
        {...tailFormItemLayout}
      >
        <Checkbox>
          Tôi đồng ý với <a href="">điều khoản</a>
        </Checkbox>
      </Form.Item>
      <Form.Item {...tailFormItemLayout}>
        <Button type="primary" htmlType="submit">
          Đăng ký
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Info;
