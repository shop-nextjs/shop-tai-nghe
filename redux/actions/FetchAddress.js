import axios from 'axios';


export const fetchProvince = () => {
    return(
        axios.get('http://localhost:8080/BDS/province')
    )
    
}

export const fetchDistrict = () => {
    return(
        axios.get('http://localhost:8080/BDS/district')
    )
    
}

export const fetchWard = () => {
    return (
        axios.get('http://localhost:8080/BDS/ward')
    )
}

