import { Col } from 'antd'
import React from 'react'
import FormRegisterAccount from './components/FormRegisterAccount'

export default function registeraccount() {
  return (
    <>
    <Col span={24}>
        <h2>Đăng ký tài khoản</h2>
        <hr></hr>
    </Col>
    <Col span={24} style={{marginTop:'4rem'}}>
        <FormRegisterAccount></FormRegisterAccount>
    </Col>
    </>
  )
}
