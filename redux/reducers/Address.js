import axios from 'axios';
import { fetchProvince , fetchDistrict , fetchWard } from '../actions/FetchAddress';
const initialState = {
    province:[],
    district:[],
    ward:[],
}

fetchProvince().then((data) => (initialState.province = data.data))
fetchDistrict().then((data) => (initialState.district = data.data));
fetchWard().then((data) => (initialState.ward = data.data));

const Address = (state =initialState,action) => {
    switch (action.type) {
        default:
            return state;
    }
}

export default Address;