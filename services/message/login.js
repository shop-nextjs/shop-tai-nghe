import { message } from "antd";

const key = "updatable";

//Chưa điền đầy đủ tên tài khoản và mật khẩu
export const NotPassed = () => {
  message.error({
    content: "Vui lòng nhập đầy đủ tên đăng nhập và mật khẩu!",
  });
};

//Đăng nhập thất bại
export const loginError = () => {
  message.loading({
    content: "Loading...",
    key,
  });
  setTimeout(() => {
    message.error({
      content: "Tài khoản hoặc mật khẩu không chính xác!",
      key,
      duration: 2,
    });
  }, 1500);
};

//Đăng nhập thành công
export const loginSuccess = () => {
  message.loading({
    content: "Loading...",
    key,
  });
  setTimeout(() => {
    message.success({
      content: "Đăng nhập thành công!",
      key,
      duration: 2,
    });
  }, 1500);
};
