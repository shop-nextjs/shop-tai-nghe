import { Button, Col, Row } from "antd";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { getCookie } from "react-use-cookie";
import FormInfo from "./components/FormInfo";
import { postOrderSuccess } from '../services/message/order';
import { postOrder } from "../services/api/order";
import { postOrderLists } from "../services/api/order";

export default function SendOrder() {
  const { shoppingCart } = useSelector((reduxData) => reduxData.taskEvent);
  let lastPrice = 0;
  shoppingCart.map((info) => {
    lastPrice = lastPrice + info.buyPrice * info.quantity;
  });
  const getToken = getCookie("token");
  const getCustomerId  = getCookie("customerId");
  const postOrderInfo = {}
  const customer = {
    id:getCustomerId
  }
  const sendOrder = () => {
    postOrderInfo.customer = customer;
    postOrder(postOrderInfo).then((result) => 
    shoppingCart.map((data) => {  
      var orderDetail = {
        quantityOrder: data.quantity,
        priceEach: data.buyPrice,
        product: {
          id:data.id
        }
      }
      postOrderLists(orderDetail,result.data.id);
      postOrderSuccess();
    }))
    
  }
  if (getToken) {
    return (
      <>
        <Col span={24}>
          <h2>Xác nhận đơn hàng</h2>
          <h1 style={{ color: "red" }}>
            Tổng tiền : {lastPrice.toLocaleString("vi-VN")} VNĐ
          </h1>
          <Button type="primary" onClick={() => sendOrder()}> Gửi đơn hàng</Button>
          <hr></hr>
        </Col>
        <Row style={{ marginTop: "5rem" }}>
          <Col span={16}>
            {shoppingCart.map((data) => {
              return (
                <Row style={{ marginTop: "1rem" }}>
                  <Col md={{ span: 14 }}>
                    <img
                      src={data.photoLink}
                      alt="product"
                      style={{ width: "100%" }}
                    ></img>
                  </Col>
                  <Col md={{ span: 8, offset: 2 }} align="left">
                    <h2>{data.productName}</h2>
                    <h4>Mã sản phẩm: {data.productCode}</h4>
                    <h4> Số lượng : {data.quantity}</h4>
                    <p>Mô tả : {data.productDescripttion}</p>
                    <h4>
                      Tổng tiền :{" "}
                      {(data.quantity * data.buyPrice).toLocaleString("vi-VN")}{" "}
                      VNĐ
                    </h4>
                  </Col>
                </Row>
              );
            })}
          </Col>
          <Col md={{ span: 6, offset: 2 }} style={{border:'1px solid #000'}} align='middle'>
            <Col style={{margin:'1rem'}}>
                <FormInfo></FormInfo>
                <Button style={{marginTop:'1rem'}} type='danger'>Nạp thêm</Button>
            </Col>
          </Col>
        </Row>
      </>
    );
  } else {
    return (
        <>
        <Col span={24}>
          <h2>Xác nhận đơn hàng</h2>
          <hr></hr>
        </Col>
        <Col span={24} align='middle' style={{marginTop:'10rem'}}>
            <h1>Bạn phải đăng nhập để gửi đơn hàng .</h1>
        </Col>
        </>
    )
  }
}
