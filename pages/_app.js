import "../styles/globals.css";
import MyNavigation from "./components/MyNavigation";
import { Col, Row } from "antd";
import { Provider } from "react-redux"; 
import configureStore, { history } from '../redux/store';
import PageRight from "./components/pageright";
import PageLeft from "./components/pageleft";
import Footer from "./components/footer";
export const store = configureStore();

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Provider store={store}>
        <div style={{backgroundColor:'rgb(233 233 233)',position:'fixed',height:'100vh',width:'100%'}}>
        </div>
        <MyNavigation></MyNavigation>
        <Row>
          <Col span={5}><PageLeft></PageLeft></Col>
          <Col md={{ span: 14}} style={{ marginTop: "10rem" }}>
            <Component {...pageProps} />
          </Col>
          <Col md={{span:5}}><PageRight></PageRight></Col>
        </Row>
        <Footer></Footer>
        
      </Provider>

    </>
  );
}

export default MyApp;