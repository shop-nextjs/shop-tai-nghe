import { message } from "antd";

const key = "updatable";

//Không nhập đầy đủ thông tin
export const warning = () => {
  message.warning({
    content: "Vui lòng nhập đầy đủ thông tin!",
  });
};

//Đăng ký thành công
export const registerSuccess = () => {
  message.loading({
    content: "Loading...",
    key,
  });
  setTimeout(() => {
    message.success({
      content: "Đăng ký thành công!",
      key,
      duration: 2,
    });
  }, 1000);
};

//Đăng ký thất bại
export const registerError = () => {
  message.loading({
    content: "Loading...",
    key,
  });
  setTimeout(() => {
    message.error({
      content: "Tài khoản đã tồn tại!",
      key,
      duration: 2,
    });
  }, 1000);
};
