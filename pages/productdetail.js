import React from "react";
import { Button, Col, Row } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { addProductToCart } from "../redux/actions/CrudProduct";
export default function ProductDetail() {

  const dispatch = useDispatch();

  const { viewProduct } = useSelector(
    (reduxData) => reduxData.taskReducer
  );

  return (
    <Row>
      <Col md={{ span: 10 }}>
        <img src={viewProduct.photoLink} style={{width:'100%'}}></img>
      </Col>
      <Col md={{ span: 10, offset: 2 }}>
        <h2>{viewProduct.productName} ( {viewProduct.productDescripttion} )</h2>
        <p>Mã sản phẩm : {viewProduct.productCode} </p>
        <p>Trong kho còn : {viewProduct.quantityInStock} sản phẩm</p>
        <h1> Giá : {viewProduct.buyPrice} VNĐ</h1>
        <Button onClick={() => dispatch(addProductToCart(viewProduct))}>Thêm vào giỏ hàng</Button>
      </Col>
    </Row>
  );
}
