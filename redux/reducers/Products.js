import {
  FETCH_PRODUCTS_PENDING,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_ERROR,
} from "../../constants/ActionTypes";
import axios from "axios";
// import { fetchProducts } from '../actions/FetchProduct';
// import { useDispatch } from "react-redux";
const initialState = {
  pending: false,
  products: [],
  error: null,
};
const loadData = () => {
  axios
    .get("http://localhost:8080/SHOP/products")
    .then((data) => (initialState.products = data.data));
};
loadData();
// const dispatch = useDispatch();
// dispatch(fetchProducts());
const Products = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS_PENDING:
      return {
        pending: true,
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        pending: false,
        products: action.products,
      };
    case FETCH_PRODUCTS_ERROR:
      return {
        pending: false,
        error: action.error,
      };
    default:
      return state;
  }
};
export default Products;
