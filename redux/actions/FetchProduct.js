import {
  FETCH_PRODUCTS_PENDING,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_ERROR,
} from "../../constants/ActionTypes";

const fetchProductsPending = () => {
  return (dispatch) => {
    dispatch({
      type: FETCH_PRODUCTS_PENDING,
    });
  };
};

const fetchProductsSuccess = (product) => {
  return (dispatch) => {
    dispatch({
      type: FETCH_PRODUCTS_SUCCESS,
      product: product,
    }); 
  };
};

const fetchProductsError = (error) => {
  return (dispatch) => {
    dispatch({
      type: FETCH_PRODUCTS_ERROR,
      error: error,
    });
  };
};

export const fetchProducts = () => {
  return (dispatch) => {
    dispatch(fetchProductsPending());
    fetch("https://localhost:8080/SHOP/products")
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          throw res.error;
        }
        dispatch(fetchProductsSuccess(res));
      })
      .catch((error) => {
        dispatch(fetchProductsError(error));
      });
  };
};
