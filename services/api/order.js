import axios from "axios";

export const postOrder = (data) => {
    return (
        axios.post('http://localhost:8080/SHOP/orders',data)
    )
}

export const postOrderLists = (data,orderId) => {
    return (
        axios.post('http://localhost:8080/SHOP/orderdetails/orderId/' + orderId ,data)
    )
}