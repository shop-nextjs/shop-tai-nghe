import React from "react";
import "antd/dist/antd.css";
import { Button , Form, Input , message } from "antd";
import { login } from "../../services/api/login";
import useCookie from 'react-use-cookie';
import { NotPassed , loginError , loginSuccess } from "../../services/message/login";

const FormLogin = () => {

  //Hook use cookie
  const [token,setToken] = useCookie('token');
  const [username,setUsername] = useCookie('username');

  //Action khi nhập đầy đủ thông tin tên đăng nhập và mật khẩu
  const onFinish = (values) => {
    setUsername(values.username);
    login(values).then((data) => {setToken(data.data);loginSuccess()}).catch(loginError())
  };
  
  //Action khi chưa nhập đầy đủ thông tin tên đăng nhập và mật khẩu
  const onFinishFailed = (errorInfo) => {
    NotPassed();
  };

  return (
    <>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        style={{ width: "100%" }}
      >
        <Form.Item
          label="Tên đăng nhập"
          name="username"
          rules={[
            {
              required: true,
              message: "Hãy điền tên đăng nhập!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Mật khẩu"
          name="password"
          rules={[
            {
              required: true,
              message: "Hãy điền mật khẩu!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Đăng nhập
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default FormLogin;
