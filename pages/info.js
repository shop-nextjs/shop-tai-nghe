import { Col  } from "antd";
import React from "react";
import { getCookie } from "react-use-cookie";
import FormInfo from "./components/FormInfo";

export default function info() {
  const getToken = getCookie("token");
  if (getToken) {
    return (
      <>
        <Col span={24}>
          <h2>Thông tin tài khoản</h2>
          <hr></hr>
        </Col>
        <FormInfo></FormInfo>
      </>
    );
  } else {
    return (
        <>
        <Col span={24}>
          <h2>Thông tin tài khoản của bạn</h2>
          <hr></hr>
        </Col>
        <Col span={24} align='middle' style={{marginTop:'10rem'}}>
            <h1>Vui lòng đăng nhập</h1>
        </Col>
        </>
    )
  }
}
