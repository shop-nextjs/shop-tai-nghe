import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Col, Row, Card, Button } from "antd";
import Link from "next/link";
import { addProductToCart , viewProduct } from "../../redux/actions/CrudProduct";
import axios from "axios";

export default function ListProducts({ listName }) {

  const [products, setProducts] = useState([]);

  const loadData = async () => {
     const response = await axios.get("http://localhost:8080/SHOP/products");
     return response.data;
  };


  const dispatch = useDispatch();
  
  useEffect(() => {
    loadData().then((data) => {
      setProducts(data);
    })
  })

  return (
    <Row>
      <Col span={24}>
        <h1>{listName}</h1>
        <hr></hr>
        <Row>
        {products.map((info) => {
            if (info.productType == listName) {
              return (
                <Col md={{ span: 7, offset: 1 }} key={info.id}>
                  <Card
                    title={info.productName}
                    bordered={true}
                    style={{ marginTop: "16px" }}
                    align="middle"
                  >
                    <img
                      src={info.photoLink}
                      style={{ width: "100%" }}
                      alt="image"
                    ></img>
                    <h2>{info.buyPrice.toLocaleString("vi-VN")} VND</h2>
                    <Link href={"/productdetail"}>
                      <Button
                        style={{ float: "left" }}
                        onClick={() => dispatch(viewProduct(info))}
                        type='danger'
                      >
                        Xem chi tiết
                      </Button>
                    </Link>
                    <Button
                      style={{ float: "right" }}
                      onClick={() => dispatch(addProductToCart(info))}
                      type="primary"
                    >
                      Thêm vào giỏ hàng
                    </Button>
                  </Card>
                </Col>
              );
            }
          })}
        </Row>
      </Col>
    </Row>
  );
}
