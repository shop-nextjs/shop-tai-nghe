import React from "react";
import { Carousel } from "antd";

export default function PageLeft() {
  const contentStyle = {
    height: "200px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
  };
  return (
    <div style={{ position: "fixed", top: "500px", width: "350px" }}>
      <Carousel autoplay="true">
        <div>
          <h3 style={contentStyle}>
            <img
              src="https://compumax.com.vn/upload/tra-gop-mpos.jpg"
              style={{ width: "100%", height: "100%" }}
            ></img>
          </h3>
        </div>
        <div>
          <h3 style={contentStyle}>
            <img
              src="https://cdn.tgdd.vn/Files/2020/04/09/1247830/th_800x450.jpg"
              style={{ width: "100%", height: "100%" }}
            ></img>
          </h3>
        </div>
        <div>
          <h3 style={contentStyle}>
            <img
              src="https://i.ytimg.com/vi/GbcjjwCN0W8/mqdefault.jpg"
              style={{ width: "100%", height: "100%" }}
            ></img>
          </h3>
        </div>
      </Carousel>
    </div>
  );
}
