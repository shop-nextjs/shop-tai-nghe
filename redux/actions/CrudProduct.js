import {
  ADD_PRODUCT_TO_CART,
  REMOVE_PRODUCT_IN_CART,
  DELETE_PRODUCT_IN_CART,
  VIEW_PRODUCT,
} from "../../constants/ActionTypes";

export const addProductToCart = (product) => {
  return (dispatch) => {
    dispatch({
      type: ADD_PRODUCT_TO_CART,
      product: product,
    });
  };
};

export const viewProduct = (product) => {
  return (dispatch) => {
    dispatch({
      type: VIEW_PRODUCT,
      product: product,
    });
  };
};

export const deleteProduct = (product) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_PRODUCT_IN_CART,
      product: product,
    });
  };
};

export const removeProduct = (product) => {
  return (dispatch) => {
    dispatch({
      type: REMOVE_PRODUCT_IN_CART,
      product: product,
    });
  };
};
