import { message } from "antd";

const key = "updatable";

//Không nhập đầy đủ thông tin
export const warning = () => {
  message.warning({
    content: "Vui lòng nhập đầy đủ thông tin!",
  });
};

//Đăng ký thành công
export const updateInfoSuccess = () => {
  message.loading({
    content: "Loading...",
    key,
  });
  setTimeout(() => {
    message.success({
      content: "Cập nhật thông tin tài khoản thành công!",
      key,
      duration: 2,
    });
  }, 1000);
};

//Đăng ký thất bại
export const updateInfoError = () => {
  message.loading({
    content: "Loading...",
    key,
  });
  setTimeout(() => {
    message.error({
      content: "Cập nhật thông tin tài khoản thất bại!",
      key,
      duration: 2,
    });
  }, 1000);
};
