import React, { useState } from "react";
import "antd/dist/antd.css";
import {
  MenuOutlined,
  ShoppingCartOutlined,
  UserOutlined,
  UserAddOutlined,
  SolutionOutlined,
  IdcardOutlined,
} from "@ant-design/icons";
import { Menu, Modal, Button } from "antd";
import Link from "next/link";
import { useSelector } from "react-redux";
import FormLogin from "./FormLogin";
import { removeCookies } from "cookies-next";

const MyNavigation = () => {
  //Các hằng số
  const { shoppingCart } = useSelector((reduxData) => reduxData.taskEvent);
  const [visible, setVisible] = useState(false);
  const [current, setCurrent] = useState("Tai nghe có dây");

  //Hiển thị số sản phẩm trong giỏ hàng
  const count = 0;
  for (let i = 0; i < shoppingCart.length; i++) {
    count = count + shoppingCart[i].quantity;
  }

  //Hành động cho modal
  const handleCancel = () => {
    setVisible(false);
  };

  //Nút click cho menu
  const onClick = (e) => {
    setCurrent(e.key);
  };

  //Khai báo cấu trúc menu
  const items = [
    {
      label: (
        <Link href="/tatcatainghe">
          <a>Danh sách sản phẩm</a>
        </Link>
      ),
      key: "List Product",
      icon: <MenuOutlined />,
      children: [
        {
          type: "item",
          key: "Tai nghe có dây",
          label: (
            <Link href="/tainghecoday">
              <a>Tai nghe có dây</a>
            </Link>
          ),
        },
        {
          type: "item",
          key: "Tai nghe không dây",
          label: (
            <Link href="/tainghekhongday">
              <a>Tai nghe không dây</a>
            </Link>
          ),
        },
        {
          type: "item",
          key: "Tai nghe chụp tai",
          label: (
            <Link href="/tainghechuptai">
              <a>Tai nghe chụp tai</a>
            </Link>
          ),
        },
        {
          type: "item",
          key: "Loa Bluetooth",
          label: (
            <Link href="/loabluetooth">
              <a>Loa Bluetooth</a>
            </Link>
          ),
        },
        {
          type: "item",
          key: "Cáp chuyển Audio",
          label: (
            <Link href="/capchuyenaudio">
              <a>Cáp chuyển Audio</a>
            </Link>
          ),
        },
        {
          type: "item",
          key: "Phụ kiện âm thanh",
          label: (
            <Link href="/phukienamthanh">
              <a>Phụ kiện âm thanh</a>
            </Link>
          ),
        },
      ],
    },
    {
      key: "Shopping Cart",
      icon: <ShoppingCartOutlined />,
      label: (
        <Link href="/cart">
          <a>
            Giỏ hàng
            <span
              style={{ color: "red", fontSize: "1.5rem", marginLeft: "1rem" }}
            >
              {count}
            </span>
          </a>
        </Link>
      ),
    },
    {
      key: "Đăng nhập",
      icon: <UserOutlined />,
      label: "Đăng nhập",
      onClick: () => setVisible(true),
    },
    {
      key: "Đăng xuất",
      icon: <UserOutlined />,
      label: "Đăng xuất",
      onClick: () => {
        removeCookies('token');
        removeCookies('username');
      }
    },
    {
      key: "Đăng ký tài khoản",
      icon: <UserAddOutlined />,
      label: (
        <Link href="/registeraccount">
          <a>Đăng ký</a>
        </Link>
      ),
    },
    {
      key: "Thông tin tài khoản",
      icon: <IdcardOutlined />,
      label: (
        <Link href="/info">
          <a>Thông tin tài khoản</a>
        </Link>
      ),
      children: [
        {
          key: "Đăng ký thông tin tài khoản",
          icon: <SolutionOutlined />,
          label: (
            <Link href="/registerinfo">
              <a>Đăng ký thông tin</a>
            </Link>
          ),
        },
      ],
    },
  ];

  return (
    <>
      <Modal
        title="Đăng nhập"
        style={{ width: "800px" }}
        visible={visible}
        onCancel={handleCancel}
        footer={
          <Button onClick={handleCancel} type="danger">
            Quay lại
          </Button>
        }
      >
        <FormLogin></FormLogin>
      </Modal>
      <Menu
        onClick={onClick}
        selectedKeys={[current]}
        mode="horizontal"
        items={items}
      />
    </>
  );
};

export default MyNavigation;
