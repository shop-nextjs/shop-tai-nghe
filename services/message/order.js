import { message } from "antd";

const key = "updatable";

//Gửi thất bại
export const postOrderError = () => {
    message.loading({
      content: "Loading...",
      key,
    });
    setTimeout(() => {
      message.error({
        content: "Gửi đơn hàng thất bại",
        key,
        duration: 2,
      });
    }, 1500);
  };


  //Gửi đơn hàng thành công
export const postOrderSuccess = () => {
    message.loading({
      content: "Loading...",
      key,
    });
    setTimeout(() => {
      message.success({
        content: "Giả vờ như đã gửi đơn hàng thành công :))) !",
        key,
        duration: 2,
      });
    }, 2000);
  };
  