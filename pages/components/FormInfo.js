import { Row , Col, Input } from 'antd';
import React, { useEffect, useState } from 'react'
import { getCookie } from 'react-use-cookie';
import { getInfo } from "../../services/api/getInfo";
import useCookie from 'react-use-cookie';

export default function FormInfo() {
    const [customerId,setCustomerId] = useCookie('customerId'); 
    const getUsername = getCookie("username");
    const [info,setInfo] = useState({});
  useEffect(() => {
    loadData();
  },[]);
  const loadData = () => {
    getInfo(getUsername).then((data) => {setInfo(data.data);setCustomerId(data.data.id)})
  }
  return (
    <>
    <Row  style={{marginTop:'2rem'}}>
          <Col md={{ span: 8 }} align='right'>
            <label>Họ và tên :</label>
          </Col>
          <Col md={{ span: 14, offset: 2 }}>
            <Input value={ info.firstName + " " + info.lastName }></Input>
          </Col>
        </Row>
        <Row  style={{marginTop:'2rem'}}>
          <Col md={{ span: 8 }} align='right'>
            <label>Địa chỉ Email :</label>
          </Col>
          <Col md={{ span: 14, offset: 2 }}>
            <Input value={ info.email }></Input>
          </Col>
        </Row>
        <Row  style={{marginTop:'2rem'}}>
          <Col md={{ span: 8 }} align='right'>
            <label>Giới tính :</label>
          </Col>
          <Col md={{ span: 14, offset: 2 }}>
            <Input value={ info.gender }></Input>
          </Col>
        </Row>
        <Row  style={{marginTop:'2rem'}}>
          <Col md={{ span: 8 }} align='right'>
            <label>Địa chỉ :</label>
          </Col>
          <Col md={{ span: 14, offset: 2 }}>
            <Input value={ info.address }></Input>
          </Col>
        </Row>
        <Row  style={{marginTop:'2rem'}}>
          <Col md={{ span: 8 }} align='right'>
            <label>Số điện thoại :</label>
          </Col>
          <Col md={{ span: 14, offset: 2 }}>
            <Input value={ info.phoneNumber }></Input>
          </Col>
        </Row>
        <Row  style={{marginTop:'2rem'}}>
          <Col md={{ span: 8 }} align='right'>
            <label>Số dư :</label>
          </Col>
          <Col md={{ span: 14, offset: 2 }}>
            <Input value={info.creditLimit} ></Input>
          </Col>
        </Row>
    </>
  )
}
