import { createStore , combineReducers , applyMiddleware } from "redux";
import Products from "../../redux/reducers/Products";
import TaskEvent from '../../redux/reducers/TaskEvent'
import thunk from "redux-thunk" 

const appReducer = combineReducers({
    taskReducer:TaskEvent,
    products:Products,
})

const store = createStore(
    appReducer,
    applyMiddleware(thunk),
    undefined
);

export default store;       

