import { Button, Col } from "antd";
import React, { useEffect, useState } from "react";
import { getCookie } from "react-use-cookie";
import FormRegisterInfo from "./components/FormRegisterInfo";
import { getInfo } from "../services/api/getInfo";
import Link from "next/link";

export default function SendOrder() {
  const getToken = getCookie("token");
  const getUsername = getCookie("username");
  const [info, setInfo] = useState({});
  useEffect(() => {
    loadData();
  }, []);
  const loadData = () => {
    getInfo(getUsername).then((data) => setInfo(data.data));
  };
  if (!getToken) {
    return (
      <>
        <Col span={24} align="left" style={{ marginBottom: "2rem" }}>
          <h2>Cập nhật thông tin cho tài khoản của bạn</h2>
          <hr></hr>
        </Col>
        <Col span={24} align="middle" style={{ marginTop: "10rem" }}>
          <h1>Vui lòng đăng nhập</h1>
        </Col>
      </>
    );
  } else {
    if (info) {
      return (
      <>
        <Col span={24} align="left" style={{ marginBottom: "2rem" }}>
          <h2>Cập nhật thông tin cho tài khoản của bạn</h2>
          <hr></hr>
        </Col>
        <Col span={24} align="middle" style={{ marginTop: "10rem" }}>
          <h1>Tài khoản của bạn đã đăng ký thông tin</h1>
        </Col>
        <Col span={24} align="right" style={{ marginTop: "10rem" }}>
          <Link href="/info"><a><Button type="primary">Xem thông tin tài khoản</Button></a></Link>
        </Col>
      </>
      )
    } else {
      return (
        <>
          <Col span={24} align="left" style={{ marginBottom: "2rem" }}>
            <h2>Cập nhật thông tin cho tài khoản của bạn</h2>
            <hr></hr>
          </Col>
          <FormRegisterInfo></FormRegisterInfo>
        </>
      );
    }
  }
}
