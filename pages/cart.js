import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { addProductToCart , deleteProduct , removeProduct } from "../redux/actions/CrudProduct";
import { Button, Col, Row } from "antd";
import Link from "next/link";

export default function cart() {
  const dispatch = useDispatch();
  const { shoppingCart } = useSelector(
    (reduxData) => reduxData.taskEvent
  );
  let lastPrice = 0;
  shoppingCart.map((info) => {
    lastPrice = lastPrice + info.buyPrice * info.quantity;
  });
  if(lastPrice == 0){
    return (
    <>
      <h1>Danh sách sản phẩm</h1>
      <hr></hr>
      <Col align='middle' span={24} style={{marginTop:'10rem'}}>
          <h1>Chưa có sản phẩm nào được thêm vào giỏ hàng</h1>
      </Col>
    </>
    )
  }
  return (
    <>
      <h1>Danh sách sản phẩm</h1>
      <hr></hr>
      <Col span={24} align="right">
        <h2> Tổng tiền : {lastPrice.toLocaleString("vi-VN")}</h2>
      </Col>
      <Col span={24} align='right'><Link href="/xacnhandonhang"><a><Button type="primary" >Xác nhận đơn hàng</Button></a></Link></Col>
      {shoppingCart.map((data) => {
        return (
          <Row style={{ marginTop: "1rem" }}>
            <Col md={{ span: 6 }}>
              <img
                src={data.photoLink}
                alt="product"
                style={{ width: "100%" }}
              ></img>
            </Col>
            <Col md={{ span: 6, offset: 1 }} align="left">
              <h2>{data.productName}</h2>
              <h4>Mã sản phẩm: {data.productCode}</h4>
              <p>{data.productDescripttion}</p>
            </Col>
            <Col
              md={{ span: 4, offset: 1 }}
              style={{ display: "inline-block" }}
              align='middle'
            >
              <Button  onClick={() => dispatch(addProductToCart(data))}>
                Thêm
              </Button>
              <h2>{data.quantity}</h2>
              <Button  onClick={() => dispatch(deleteProduct(data))}>Bớt</Button>
              <Button type="danger" onClick={() => dispatch(removeProduct(data))} style={{marginTop:'2rem'}}>
                Xoá sản phẩm khỏi giỏ hàng
              </Button>
            </Col>
            <Col md={{ span: 4, offset: 2 }}>
              <h3>
                Giá : {(data.buyPrice * data.quantity).toLocaleString("vi-VN")}{" "}
                VNĐ{" "}
              </h3>
            </Col>
          </Row>
        );
      })}
    </>
  );
}
