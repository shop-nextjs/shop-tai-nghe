import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import TaskEvent from "./TaskEvent";
import Products from "./Products";
import Address from './Address';

export default (history) =>
  combineReducers({
  router: connectRouter(history),
  taskEvent: TaskEvent,
  products: Products,
  address:Address,
}); 
